import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { registerApplication, start } from 'single-spa'

const loadScript = async (url) => {
  await new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.src = url
    script.onload = resolve
    script.onerror = reject
    document.head.appendChild(script)
  })
}

Vue.config.productionTip = false

/**
 * singleSpa 缺陷
 * 1、不够灵活，不能动态加载JS文件
 * 2、样式不隔离，没有JS沙箱的机制
 */
registerApplication('myVueApp',
  async () => {
    console.log('加载模块')
    await loadScript('http://localhost:10001/js/chunk-vendors.js')
    await loadScript('http://localhost:10001/js/app.js')
    return window.singleVue
  },
  location => location.pathname.startsWith('/vue') // 用户切换到/vue的路径下，需要加载子应用
)
start()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
